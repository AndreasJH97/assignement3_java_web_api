<!-- ABOUT THE PROJECT -->
## Access And Expose A Database
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://gitlab.com/AndersRingen/chinook/-/blob/main/README.md)
[![pipeline-satus](https://gitlab.com/AndreasJH97/assignement3_java_web_api/badges/main/pipeline.svg)]()

This is the two-part project where Andreas and Anders develop their skills in Spring Web, REST API, JPA, DTO, IRL, LMAO, Hibernate and the rest of it. 


### Built With
This is made with Java and PostgreSQL in the Intellij IDE, and PGAdmin as a database helper tool.
* [Java](https://www.java.com/en/)
* [Intellij](https://www.jetbrains.com/idea/)
* [PostgreSQL](https://www.postgresql.org/)
* [PGAdmin](https://www.pgadmin.org/)


<!-- USAGE EXAMPLES -->

# Explaination

### Pt.1: Hibernate

You have been tasked with creating a datastore and interface to store and manipulate movie characters. It may be
expanded over time to include other digital media, but for now, stick to movies.
The application should be constructed in Spring Web and comprise of a database made in PostgreSQL through Hibernate
with a RESTful API to allow users to manipulate the data. The database will store information about characters, movies
they appear in, and the franchises these movies belong to. This should be able to be expanded on.

### Pt.2: Web API Using Spring Web
This section and subsections detail the requirements of the endpoints of the system on a high level. The
naming of these endpoints and the controller they are contained in is up to you to decide and forms part of
the convention mark.

*Soon to be fullstack world champions.*
