--Insert movies
INSERT INTO movie (movie_title, genre, year, director, poster_url, trailer_url) VALUES('Tigers Wood','Sports',2000,'Anders Ringen','oink','oink')
INSERT INTO movie (movie_title, genre, year, director, poster_url, trailer_url) VALUES('Ole Brum','Cartoon',1995,'Erlend Loe','testUrl','trailerUrl')
INSERT INTO movie (movie_title, genre, year, director, poster_url, trailer_url) VALUES('Spider-Man: Homecoming','Action',2007,'Sam Raimi','spidermanPoster','Spiderman Trailer')

-- --Insert Characters
INSERT INTO karakter (name, alias, gender, img_url) VALUES ('Drunkman','John Sober Smith','male','drunkmanImg')
INSERT INTO karakter (name, alias, gender, img_url) VALUES ('Spider-Man','Peter Parker','male','spidermanImg')
INSERT INTO karakter (name, alias, gender, img_url) VALUES ('Batman','Bruce Wayne','male','batmanUrl')

-- --Insert Frachises
INSERT INTO franchise (name, description) VALUES ('Spiderman franchise', 'Superhuman strength, agility, endurance, ability to stick to and climb walls and other surfaces, uses self-designed web-shooters allowing him to fire and swing from sticky webs, special "Spider-Sense" warns of incoming danger, genius intellect specializing in chemistry and invention')
INSERT INTO franchise (name, description) VALUES ('Batman franchise', 'Batman was originally introduced as a ruthless vigilante who frequently killed or maimed criminals, but evolved into a character with a stringent moral code and strong sense of justice. Unlike most superheroes, Batman does not possess any superpowers, instead relying on his intellect, fighting skills, and wealth.')
INSERT INTO franchise (name, description) VALUES ('Harry Potter', 'Throughout the series, Harry is described as having his father''s perpetually untidy black hair, his mother''s bright green eyes, and a lightning bolt-shaped scar on his forehead. He is further described as "small and skinny for his age" with "a thin face" and "knobbly knees", and he wears Windsor glasses.')