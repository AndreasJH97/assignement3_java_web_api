package no.noroff.accelerate.java_web_api.services.character;

import no.noroff.accelerate.java_web_api.models.Karakter;
import no.noroff.accelerate.java_web_api.services.CrudService;

public interface CharacterService extends CrudService<Karakter, Integer> {

}
