package no.noroff.accelerate.java_web_api.services.movie;
import no.noroff.accelerate.java_web_api.models.Karakter;
import no.noroff.accelerate.java_web_api.models.Movie;
import no.noroff.accelerate.java_web_api.models.dtos.MovieDTO;
import no.noroff.accelerate.java_web_api.services.CrudService;

import java.util.Collection;
import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {

    // Extra business logic
    Movie setCharactersInMovie(int[] character_ids, int movie_id);

    Set<Karakter> findCharactersInMovie(int movie_id);

    Set<Karakter> findCharactersInMovies(Collection<Movie> movieDTOS);
}
