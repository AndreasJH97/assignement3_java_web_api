package no.noroff.accelerate.java_web_api.services.franchise;

import no.noroff.accelerate.java_web_api.exeptions.FranchiseNotFoundException;
import no.noroff.accelerate.java_web_api.models.Franchise;
import no.noroff.accelerate.java_web_api.models.Movie;
import no.noroff.accelerate.java_web_api.repositories.FranchiseRepository;
import no.noroff.accelerate.java_web_api.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    // This class contains mostly the use of the repository that uses methods from JPArepository
    // which handles the queries for adding, updating, deleting and retrieving data
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }
    // These methods return the default methods that franchiserepository gets from JpaRepositories
    @Override
    public Franchise findById(Integer integer) { //Throws NotFoundException(404) if object doesnt exist
        return franchiseRepository.findById(integer).orElseThrow(() -> new FranchiseNotFoundException(integer));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override // When adding a franchise with movies, one has to through all the movies and add the franchise
    public Franchise add(Franchise entity) { // because it is an OneToMany relationship, and it is the movies
        entity.getMovies().forEach(m -> m.setFranchise(entity));// that is the owner of the foreign key
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        franchiseRepository.deleteById(integer);
    }

    @Override
    public Franchise setMovies(int franchise_id, int[] movie_id) {
        Franchise franchise = franchiseRepository.findById(franchise_id).get();
        Set<Movie> movies = new HashSet<>();
        for (int i = 0; i < movie_id.length; i++) {
            Movie movie = movieRepository.findById(movie_id[i]).get();
            movies.add(movie);
        }// this is the same operation as in the add method. Adding the franchise to all its movies.
        movies.forEach(m -> m.setFranchise(franchise));
        return franchiseRepository.save(franchise);
    }
    @Override
    public Set<Movie> findMovies(int franchise_id) {
        Franchise franchise = franchiseRepository.findById(franchise_id).get();
        return franchise.getMovies(); // Retrieving the movies to a franchise
    }
}