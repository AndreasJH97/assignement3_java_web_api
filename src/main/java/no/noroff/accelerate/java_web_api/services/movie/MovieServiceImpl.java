package no.noroff.accelerate.java_web_api.services.movie;

import no.noroff.accelerate.java_web_api.exeptions.MovieNotFoundException;
import no.noroff.accelerate.java_web_api.models.Karakter;
import no.noroff.accelerate.java_web_api.models.Movie;
import no.noroff.accelerate.java_web_api.repositories.CharacterRepository;
import no.noroff.accelerate.java_web_api.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MovieServiceImpl implements MovieService{
    // This class contains mostly the use of the repository that uses methods from JPArepository
    // which handles the queries for adding, updating, deleting and retrieving data
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }
    // These methods return the default methods that movierepository gets from JpaRepositories
    @Override
    public Movie findById(Integer integer) {//Throws NotFoundException(404) if object doesnt exist
        return movieRepository.findById(integer).orElseThrow(() -> new MovieNotFoundException(integer));
    } // returns the movie unless the database cannot find the id

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        movieRepository.deleteById(integer);
    }

    @Override
    public Movie setCharactersInMovie(int[] character_ids,int movie_id) {
        // get movie for id from movieRepository
        // make a list of characters, loop through characters_id, get character by id with characterRepository
        // movie.setCharacters(listOfCharacters)
        // movieRepository.save(movie)
        System.out.println(Arrays.toString(Arrays.stream(character_ids).toArray()));
        System.out.println("CharArrray: "+Arrays.toString(character_ids));
        Movie movie=null;
        if (movieRepository.findById(movie_id).isPresent()) {
            movie = movieRepository.findById(movie_id).get();
        }
        Set<Karakter> characterSet = new HashSet<>();
        for(Integer i:character_ids){
            if (characterRepository.findById(i).isPresent())
                characterSet.add(characterRepository.findById(i).get());
        }
        System.out.println("CharacterSettet er: "+characterSet);
        movie.setCharacters(characterSet);
        return movieRepository.save(movie);
    }
    @Override // retrieving all the characters in a movie from the database.
    public Set<Karakter> findCharactersInMovie(int movie_id) {
        Movie movie = movieRepository.findById(movie_id).get();
        return movie.getCharacters();
    }
    @Override // Creates a new Set of all characters that are in a list of movies by looping through the movies
    public Set<Karakter> findCharactersInMovies(Collection<Movie> movies) { // and adding all its characters to the set
        Set<Karakter> characters = new HashSet<>();
        movies.forEach(m -> characters.addAll(m.getCharacters()));
        return characters;
    }
}
