package no.noroff.accelerate.java_web_api.services.franchise;

import no.noroff.accelerate.java_web_api.models.Franchise;
import no.noroff.accelerate.java_web_api.models.Movie;
import no.noroff.accelerate.java_web_api.services.CrudService;

import java.util.Collection;


public interface FranchiseService extends CrudService<Franchise, Integer> {
    // Extra business logic
    Franchise setMovies(int franchise_id, int[] movie_id);

    Collection<Movie> findMovies(int franchise_id);
}
