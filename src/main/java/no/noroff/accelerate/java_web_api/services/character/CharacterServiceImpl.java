package no.noroff.accelerate.java_web_api.services.character;
import no.noroff.accelerate.java_web_api.exeptions.CharacterNotFoundException;
import no.noroff.accelerate.java_web_api.models.Karakter;

import no.noroff.accelerate.java_web_api.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService{
    // This class contains mostly the use of the repository that uses methods from JPArepository
    // which handles the queries for adding, updating, deleting and retrieving data
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }
    // These methods return the default methods that characterrepository gets from JpaRepositories
    @Override
    public Karakter findById(Integer integer) {//Throws NotFoundException(404) if object doesnt exist
        return characterRepository.findById(integer).orElseThrow(() -> new CharacterNotFoundException(integer));
    } // Retrieves the character. If the id does not exist in the database, an exception is thrown

    @Override
    public Collection<Karakter> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Karakter add(Karakter entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Karakter update(Karakter entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        characterRepository.deleteById(integer);
    }

}
