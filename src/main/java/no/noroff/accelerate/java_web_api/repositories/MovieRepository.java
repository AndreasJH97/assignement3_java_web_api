package no.noroff.accelerate.java_web_api.repositories;

import no.noroff.accelerate.java_web_api.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie,Integer> {
}
