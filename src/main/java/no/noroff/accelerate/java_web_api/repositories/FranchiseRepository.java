package no.noroff.accelerate.java_web_api.repositories;

import no.noroff.accelerate.java_web_api.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise,Integer> {
}
