package no.noroff.accelerate.java_web_api.repositories;

import no.noroff.accelerate.java_web_api.models.Karakter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Karakter,Integer> {
}
