package no.noroff.accelerate.java_web_api.runner;

import no.noroff.accelerate.java_web_api.models.Movie;
import no.noroff.accelerate.java_web_api.repositories.MovieRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AppRunner implements ApplicationRunner {


    @Override
    public void run(ApplicationArguments args) throws Exception {

    }
}
