package no.noroff.accelerate.java_web_api.mappers;

import no.noroff.accelerate.java_web_api.models.Franchise;
import no.noroff.accelerate.java_web_api.models.Karakter;
import no.noroff.accelerate.java_web_api.models.Movie;
import no.noroff.accelerate.java_web_api.models.dtos.MovieDTO;
import no.noroff.accelerate.java_web_api.services.character.CharacterService;
import no.noroff.accelerate.java_web_api.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import no.noroff.accelerate.java_web_api.services.movie.MovieService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel="spring")
public abstract class MovieMapper {
    @Autowired
    protected MovieService movieService;
    @Autowired
    protected CharacterService characterService;

    @Autowired
    protected FranchiseService franchiseService;

    @Mapping(target= "franchise", source = "franchise.franchise_id")
    @Mapping(target= "characters", source = "characters", qualifiedByName = "charactersToInt")
    public abstract MovieDTO movieToMovieDTO(Movie movie);// this converts characters from a
                                                        // normal object to a DTO, with uses the
                                                        // qualifiedByName as the method between the source and the target
    @Named("charactersToInt")
    public Set<Integer> map1(Set<Karakter> source){
        if(source==null)// First checks if its null
            return null;// if not then go through the set to make another set with just the ids
        return source.stream().map(Karakter::getCharacter_id).collect(Collectors.toSet());
    }
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "intToFranchise")
    @Mapping(target= "characters", source = "characters", qualifiedByName = "intToCharacters")
    public abstract Movie movieDTOTomovie(MovieDTO movieDTO);

    public abstract Collection<MovieDTO> moviesToMovieDTOs(Collection<Movie> movies);

    @Named("intToFranchise")
    public Franchise intToFranchise(Integer source){
        if (source == null)
            return null;
        return franchiseService.findById(source);
    }

    @Named("intToCharacters")
    public Set<Karakter> intToCharacters(Set<Integer> source){
        if (source == null)
            return null;
        return source.stream().map(i -> characterService.findById(i)).collect(Collectors.toSet());
    }
}
