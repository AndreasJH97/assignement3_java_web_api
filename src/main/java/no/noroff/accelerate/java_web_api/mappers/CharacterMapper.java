package no.noroff.accelerate.java_web_api.mappers;
import no.noroff.accelerate.java_web_api.models.Karakter;
import no.noroff.accelerate.java_web_api.models.Movie;
import no.noroff.accelerate.java_web_api.models.dtos.CharacterDTO;
import no.noroff.accelerate.java_web_api.services.character.CharacterService;
import no.noroff.accelerate.java_web_api.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel="spring")
public abstract class CharacterMapper {
    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected MovieService movieService;
    @Mapping(target= "movies", source = "movies", qualifiedByName = "moviesToInt") // this converts characters from a
    public abstract CharacterDTO characterToCharacterDTO(Karakter karakter);// normal object to a DTO, with uses the
                                                                            // qualifiedByName as the method between the source and the target
    public abstract Collection<CharacterDTO> characterToCharacterDTOS(Collection<Karakter> characters);

    @Mapping(target= "movies", source = "movies", qualifiedByName = "intToMovies")// Converts a DTO to a normal object
    public abstract Karakter characterDTOToCharacter(CharacterDTO characterDTO);// with the specified method

    @Named("moviesToInt")
    public Set<Integer> moviesToInt(Set<Movie> source){
        if(source==null) // First checks if its null
            return null; // if not then go through the set to make another set with just the ids
        return source.stream().map(Movie::getMovie_id).collect(Collectors.toSet());
    }
    @Named("intToMovies")
    public Set<Movie> intToMovies(Set<Integer> source){
        if(source==null)// First checks if its null
            return null;// if not then go through the set and retrieve the objects by their ids
        return source.stream().map(m -> movieService.findById(m)).collect(Collectors.toSet());
    }
}
