package no.noroff.accelerate.java_web_api.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int franchise_id;

    @Column(length=100, nullable = false)
    private String name;
    @Column(length=700)
    private String description;

    ///// Navigation / Relationship //////

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
    public int getFranchise_id() {
        return franchise_id;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
