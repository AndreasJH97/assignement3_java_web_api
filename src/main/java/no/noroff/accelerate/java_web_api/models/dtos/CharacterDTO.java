package no.noroff.accelerate.java_web_api.models.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CharacterDTO {
    private int character_id;
    private String name;
    private String alias;
    private String gender;
    private String imgUrl;
    private Set<Integer> movies;
}
