package no.noroff.accelerate.java_web_api.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int movie_id;
    @Column(length=100, nullable = false)
    private String movie_title;
    @Column(length=100,nullable = false)
    private String genre;
    @Column()
    private int year;
    @Column(length=100)
    private String director;
    @Column(length=700)
    private String poster_url;
    @Column(length=200)
    private String trailer_url;


    ///// Navigation / Relationship //////
    @ManyToMany
    private Set<Karakter> characters;
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

}

