package no.noroff.accelerate.java_web_api.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Karakter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int character_id;

    @Column(length=100,nullable = false)
    private String name;
    @Column(length=100)
    private String alias;
    @Column(length=100)
    private String gender;
    @Column(length=100)
    private String imgUrl;
    ///// Navigation / Relationship //////
    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;
}
