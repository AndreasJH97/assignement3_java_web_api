package no.noroff.accelerate.java_web_api.models.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MovieDTO {
    private int movie_id;
    private String movie_title;
    private String genre;
    private int year;
    private String director;
    private String poster_url;
    private String trailer_url;
    private Set<Integer> characters;
    private Integer franchise;
}
