package no.noroff.accelerate.java_web_api.models.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class FranchiseDTO {
    private int franchise_id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
