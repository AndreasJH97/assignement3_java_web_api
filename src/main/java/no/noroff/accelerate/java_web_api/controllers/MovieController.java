package no.noroff.accelerate.java_web_api.controllers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.java_web_api.mappers.CharacterMapper;
import no.noroff.accelerate.java_web_api.mappers.MovieMapper;
import no.noroff.accelerate.java_web_api.models.dtos.CharacterDTO;
import no.noroff.accelerate.java_web_api.models.dtos.MovieDTO;
import no.noroff.accelerate.java_web_api.services.movie.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(value="api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;  // this is where we instantiate the different mappers and services
    private final CharacterMapper characterMapper;// so we can use their methods


    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }
    @Operation(summary= "Find all movies")//Description of method showed in swagger
    @ApiResponses(value={ //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200", //and what the method is expected to return if successfully used.
                    description = "Success",
                    content = {
                            @Content( //On success, return an Array of the object MovieDTO
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class))) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movies was not found",
                    content = @Content)
    })
    @GetMapping("findAll")
    public ResponseEntity getAll(){// to find all franchises, we call the findAll-method from characterService
        Collection<MovieDTO> movieDTOs = movieMapper.moviesToMovieDTOs(movieService.findAll());
        return ResponseEntity.ok(movieDTOs); //  and convert all the objects to their respective DTOs
    }
    @Operation(summary= "Add new movie")//Description of method showed in swagger
    @ApiResponses(value={//Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "201",//and what the method is expected to return if successfully used.
                    description = "Movie added",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content)
    })
    @PostMapping("add")
    public ResponseEntity add(@RequestBody MovieDTO movieDTO) {
        MovieDTO m = // The add method takes in an FranchiseDTO and then converts it to
                movieMapper.movieToMovieDTO(// a normal object, then adds it to the database before
                movieService.add(// converting the return object back to a DTO
                movieMapper.movieDTOTomovie(movieDTO)));
        URI location = URI.create("movies/"+m.getMovie_id());
        return ResponseEntity.created(location).build();
    }
    @Operation(summary= "Find movie by ID")//Description of method showed in swagger
    @ApiResponses(value={//Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",//and what the method is expected to return if successfully used.
                    description = "Success",
                    content = { @Content(mediaType = "application/json",//On success, return an object of the MovieDTO class
                            schema = @Schema(implementation = MovieDTO.class)) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie was not found",
                    content = @Content)
    })
    @GetMapping("findById/{id}")// Here is a movieDTO made by converting an extracted movie object
    public ResponseEntity getById(@PathVariable int id){
        MovieDTO movieDTO
                = movieMapper.movieToMovieDTO(
                movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary= "Get All Characters in movie with ID")//Description of method showed in swagger
    @ApiResponses(value={//Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",//and what the method is expected to return if successfully used.
                    description = "Success",
                    content = {
                            @Content(//On success, return an Array of Objects from the CharacterDTO Class
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie was not found",
                    content = @Content)
    })
    @GetMapping("findAllCharacters/{id}")// This endpoint extracts all characters in a movie
    public ResponseEntity findAllCharacters(@PathVariable int id){
        Collection<CharacterDTO> characterDTOS = characterMapper.characterToCharacterDTOS(movieService.findCharactersInMovie(id));
        return ResponseEntity.ok(characterDTOS);
    }

    @Operation(summary= "Update movie")//Description of method showed in swagger
    @ApiResponses(value={//Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "204",//and what the method is expected to return if successfully used.
                    description = "Movie was successfully updated",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie was not found",
                    content = @Content)
    })
    @PutMapping("update/{id}")
    public ResponseEntity update(@RequestBody MovieDTO movieDTO, @PathVariable int id){
        if (id != movieDTO.getMovie_id())// This is the same as in the add, just with the update method
            return ResponseEntity.badRequest().build();
        movieMapper.movieToMovieDTO(movieService.update(
                movieMapper.movieDTOTomovie(movieDTO)));
        return ResponseEntity.noContent().build();
    }
    @Operation(summary= "Delete Movie with ID")//Description of method showed in swagger
    @ApiResponses(value={//Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "204",//and what the method is expected to return if successfully used.
                    description = "Movie deleted",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie was not found",
                    content = @Content)
    })
    @DeleteMapping("deleteById/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        movieService.deleteById(id); // This deletes a movie by a given id, and does not have mess around with
        return ResponseEntity.noContent().build();// any DTO business
    }
    @Operation(summary= "Add characters in movie with ID")//Description of method showed in swagger
    @ApiResponses(value={//Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",//and what the method is expected to return if successfully used.
                    description = "Success",
                    content = { @Content(mediaType = "application/json",//On success, return an object of MovieDTO Class
                            schema = @Schema(implementation = MovieDTO.class)) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie was not found",
                    content = @Content)
    })
    @PutMapping("{id}/characters") // This endpoint adds characters to a movie
    public ResponseEntity updateMovieWithCharacters(@RequestBody int[] characterIdList, @PathVariable int id){
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.setCharactersInMovie(characterIdList, id));
        return ResponseEntity.ok(movieDTO);
    }
}
