package no.noroff.accelerate.java_web_api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.java_web_api.mappers.CharacterMapper;
import no.noroff.accelerate.java_web_api.models.dtos.CharacterDTO;
import no.noroff.accelerate.java_web_api.services.character.CharacterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.Collection;
@RestController
@RequestMapping(value="api/v1/characters")
public class CharacterController {

    private final CharacterService characterService; // this is where we instantiate the different mappers and services
    private final CharacterMapper characterMapper;// so we can use their methods

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }
    @Operation(summary= "Find all characters")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",  //and what the method is expected to return if successfully used.
                    description = "Success",
                    content = {
                            @Content(   //On success, return an Array of the object CharacterDTO
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Characters was not found",
                    content = @Content)
    })
    @GetMapping("findAll")
    public ResponseEntity getAll(){// to find all characters, we call the findAll-method from characterService
        Collection<CharacterDTO> characterDTOS = characterMapper.characterToCharacterDTOS(characterService.findAll());
        return ResponseEntity.ok(characterDTOS);//  and convert all the objects to their respective DTOs
    }
    @Operation(summary= "Find character by ID")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",  //and what the method is expected to return if successfully used.
                    description = "Success",   //On success, return an CharacterDTO object
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character was not found",
                    content = @Content)
    })
    @GetMapping("findById/{id}")// Here is a characterDTO made by converting an extracted character object
    public ResponseEntity getById(@PathVariable int id){//  using its id
        CharacterDTO characterDTO=characterMapper.characterToCharacterDTO(characterService.findById(id));
        return ResponseEntity.ok(characterDTO);
}
    @Operation(summary= "Add character")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "201",  //and what the method is expected to return if successfully used.
                    description = "Character added",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
    })
    @PostMapping("add")
    public ResponseEntity add(@RequestBody CharacterDTO karakterDTO){
        CharacterDTO k = // The add method takes in an FranchiseDTO and then converts it to
                characterMapper.characterToCharacterDTO(// a normal object, then adds it to the database before
                        characterService.add(// converting the return object back to a DTO
                characterMapper.characterDTOToCharacter(karakterDTO)));
        URI location = URI.create("characters/"+k.getCharacter_id());
        return ResponseEntity.created(location).build();
    }
    @Operation(summary= "Update character by ID")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "204",  //and what the method is expected to return if successfully used.
                    description = "Character was successfully updated",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character was not found",
                    content = @Content)
    })
    @PutMapping("update/{id}")
    public ResponseEntity update(@RequestBody CharacterDTO characterDTO, @PathVariable int id){
        if(id!=characterDTO.getCharacter_id())// This is the same as in the add, just with the update method
            return ResponseEntity.badRequest().build();
        characterMapper.characterToCharacterDTO(characterService.update(
                characterMapper.characterDTOToCharacter(characterDTO)));
        return ResponseEntity.noContent().build();
    }
    @Operation(summary= "Delete character by ID")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "204",  //and what the method is expected to return if successfully used.
                    description = "Character succsessfully deleted",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character was not found",
                    content = @Content)
    })
    @DeleteMapping("deleteById/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        characterService.deleteById(id);// This deletes a franchise by a given id, and does not have mess around with
        return ResponseEntity.noContent().build();// any DTO business
    }
}
