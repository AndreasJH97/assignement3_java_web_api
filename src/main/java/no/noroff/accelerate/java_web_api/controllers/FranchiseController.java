package no.noroff.accelerate.java_web_api.controllers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.java_web_api.mappers.CharacterMapper;
import no.noroff.accelerate.java_web_api.mappers.FranchiseMapper;
import no.noroff.accelerate.java_web_api.mappers.MovieMapper;
import no.noroff.accelerate.java_web_api.models.dtos.CharacterDTO;
import no.noroff.accelerate.java_web_api.models.dtos.FranchiseDTO;
import no.noroff.accelerate.java_web_api.models.dtos.MovieDTO;
import no.noroff.accelerate.java_web_api.services.franchise.FranchiseService;
import no.noroff.accelerate.java_web_api.services.movie.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.*;

@RestController
@RequestMapping(value="api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final MovieService movieService;
    private final FranchiseMapper franchiseMapper; // this is where we instantiate the different mappers and services
    private final MovieMapper movieMapper;         // so we can use their methods
    private final CharacterMapper characterMapper;

    public FranchiseController(FranchiseService franchiseService, MovieService movieService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.franchiseService = franchiseService;
        this.movieService = movieService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary= "Find all franchises")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",  //and what the method is expected to return if successfully used.
                    description = "Success",
                    content = {
                            @Content(//On success, return an Array of the object FranchiseDTO
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class))) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise was not found",
                    content = @Content)
    })
    @GetMapping("findAll")
    public ResponseEntity getAll(){  // to find all franchises, we call the findAll-method from characterService
        Collection<FranchiseDTO> franchiseDTOs = franchiseMapper.franchisesToFranchiseDTOs(franchiseService.findAll());
        return ResponseEntity.ok(franchiseDTOs); //  and convert all the objects to their respective DTOs
    }
    @Operation(summary= "Find franchise by ID")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200", //and what the method is expected to return if successfully used.
                    description = "Success",
                    content = { @Content(mediaType = "application/json",//On success, return an object of the FranchiseDTO Class
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise was not found",
                    content = @Content)
    })
    @GetMapping("findById/{id}")
    public ResponseEntity getById(@PathVariable int id){
        FranchiseDTO franchiseDTO // Here is a franchiseDTO made by converting an extracted franchise object
                = franchiseMapper.franchiseToFranchiseDTO( //  using its id
                franchiseService.findById(id));
        return ResponseEntity.ok(franchiseDTO);
    }
    @Operation(summary= "Add franchise")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "201",//and what the method is expected to return if successfully used.
                    description = "Franchise added",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
    })
    @PostMapping("add")
    public ResponseEntity add(@RequestBody FranchiseDTO franchiseDTO) {
        FranchiseDTO f                            // The add method takes in an FranchiseDTO and then converts it to
                = franchiseMapper.franchiseToFranchiseDTO( // a normal object, then adds it to the database before
                franchiseService.add(             // converting the return object back to a DTO
                        franchiseMapper.franchiseDTOToFranchise(franchiseDTO)));
        URI location = URI.create("franchises/"+f.getFranchise_id());
        return ResponseEntity.created(location).build();
    }
    @Operation(summary= "Update franchise by ID")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "204",//and what the method is expected to return if successfully used.
                    description = "Franchise was successfully updated",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise was not found",
                    content = @Content)
    })
    @PutMapping("update/{id}")
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id){
        if (id != franchiseDTO.getFranchise_id()) // This is the same as in the add, just with the update method
            return ResponseEntity.badRequest().build();
        franchiseMapper.franchiseToFranchiseDTO(franchiseService.update(franchiseMapper.franchiseDTOToFranchise(franchiseDTO)));
        return ResponseEntity.noContent().build();
    }
    @Operation(summary= "Delete franchise by ID")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "204",//and what the method is expected to return if successfully used.
                    description = "Franchise was successfully deleted",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise was not found",
                    content = @Content)
    })
    @DeleteMapping("deleteById/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        franchiseService.deleteById(id); // This deletes a franchise by a given id, and does not have mess around with
        return ResponseEntity.noContent().build(); // any DTO business
    }
    @Operation(summary= "Add movies in franchise")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",//and what the method is expected to return if successfully used.
                    description = "Success",//On success, return an object of FranchiseDTO with added movies
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "204",
                    description = "Franchise found",
                    content = @Content),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise was not found",
                    content = @Content)
    })
    @PutMapping("{id}/addMovies") // This endpoint adds multiple movies to a franchise
    public ResponseEntity updateFranchiseWithMovies(@RequestBody int[] movieIdList, @PathVariable int id){
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(franchiseService.setMovies(id, movieIdList));
        return ResponseEntity.ok(franchiseDTO);
    }
    @Operation(summary= "Find all movies in a fanchise")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",//and what the method is expected to return if successfully used.
                    description = "Success",
                    content = {
                            @Content(//On success, return an array of MovieDto objects
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class))) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise was not found",
                    content = @Content)
    })
    @GetMapping("{id}/movies") // This is just like the latter, by retrieves all movies related to a spesific franchise
    public ResponseEntity findMoviesInFranchise(@PathVariable int id){
        Collection<MovieDTO> movies = movieMapper.moviesToMovieDTOs(franchiseService.findMovies(id));
        return ResponseEntity.ok(movies);
    }
    @Operation(summary= "Find all characters in a franchise")  //Description of method showed in swagger
    @ApiResponses(value={   //Different Response codes the method can return, their meaning,
            @ApiResponse(responseCode = "200",//and what the method is expected to return if successfully used.
                    description = "Success",
                    content = {
                            @Content(//On success, return an array of CharacterDto objects
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse (responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise was not found",
                    content = @Content)
    })
    @GetMapping("{id}/characters") // This endpoint find every character in every movie relating to one franchise
    public ResponseEntity findCharactersInFranchise(@PathVariable int id){
        Collection<CharacterDTO> characterDTOS = characterMapper.characterToCharacterDTOS(
                movieService.findCharactersInMovies(
                        franchiseService.findMovies(id)));
        return ResponseEntity.ok(characterDTOS);
    }
}
